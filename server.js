const express = require('express')
const app = express()
const fs = require('fs')
const os = require('os')
const http = require('http')
const readline = require('readline')
const {
    PORT = 3000
} = process.env

app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`)
    reqAnswer()
})

const server = http.createServer((req, res) => {
    console.log("Server started on 5000");
    if (req.url === '/') {
        res.write('HELLO WORLD!')
        res.end()
    }
})

const readFile = () => {

    fs.readFile(__dirname + '/package.json', 'utf-8', (err, contents) => {
        console.log(contents);
        console.log(err);
    })
}

const readOS = () => {
    console.log('SYSTEM MEMORY', (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + ' GB');
}

const r1 = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})


const reqAnswer = () => {

    r1.question(`
        Choose one:
        1. Read package.json
        2. Display OS info
        3. Start HTTP server
        Type a number:
        `, function (options) {
            switch (parseInt(options)) {
                case 1:
                    readFile()
                    break
                case 2:
                    readOS()
                    break
                case 3:
                    server.listen(5000)
                    break

            }
        r1.close()
    })

}